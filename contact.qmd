---
title: Contact
---

The author, Sytse Knypstra, is a retired statistics teacher at the university of Groningen (The Netherlands). His personal website is [pyqrs.eu](https://pyqrs.eu/sk).

You can contact Sytse Knypstra at his e-mailaddress: `Sytse (at) pyqrs.eu`

The PyQRS project is hosted on [GitLab](https://gitlab.com/pyqrs).
